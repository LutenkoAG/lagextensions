//
//  UIViewController+PushAndPresent.swift
//  LAGExtensions
//
//  Created by phpFoxer on 4/18/18.
//  Copyright © 2018 LAG. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func pushVC(navigationController: UINavigationController, storyboardName: String, viewControllerName: String, animated: Bool) {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: viewControllerName)
        navigationController.pushViewController(viewController, animated: animated)
    }
    
    func presentVC(storyboardName: String, viewControllerName: String, animated: Bool) {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: viewControllerName)
        self.present(viewController, animated: true, completion: nil)
    }
}
